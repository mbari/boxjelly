---
description: BoxJelly installation and usage
---

# Intro

**BoxJelly** is a tool for viewing and editing object tracks in video.

<div align="center">
    <img style="vertical-align: middle" src="assets/images/usage/main_window.png" alt="BoxJelly window" width="48%"/>
    <img style="vertical-align: middle" src="assets/images/usage/video_window.png" alt="Cthulhu window" width="48%"/>
</div>

Source code is available on Bitbucket at [bitbucket.org/mbari/boxjelly](https://bitbucket.org/mbari/boxjelly/).

[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Python](https://img.shields.io/badge/language-Python-blue.svg)](https://www.python.org/downloads/)

Author: Kevin Barnard, [kbarnard@mbari.org](mailto:kbarnard@mbari.org)

- **To install BoxJelly, please see the instructions on the [install page](install.md).**

- Usage instructions are available on the [usage page](usage.md).

## Demo

<iframe src="https://www.youtube.com/embed/P6oeep6Z4oM" style="position: relative; width: 100%; height: 22.172vw" allowfullscreen="" frameborder="0"></iframe>

*Note: This video shows an older version of BoxJelly with the internal video player.*
