# Install

BoxJelly requires Python 3.8 or later; see [python.org/downloads](https://python.org/downloads/).

## Installing BoxJelly from PyPI

BoxJelly is available on the Python Package Index (PyPI) as `boxjelly`. To install, run:

```bash
pip install boxjelly
```

You can check which version of BoxJelly you have installed with:

```bash
pip show boxjelly
```

Updating BoxJelly is easy:

```bash
pip install --upgrade boxjelly
```

## Installing Cthulhu

BoxJelly uses the Cthulhu video player, which is available at [github.com/mbari-media-management/cthulhu](https://github.com/mbari-media-management/cthulhu/).

---

## Building BoxJelly from source

BoxJelly is built with [Poetry](https://python-poetry.org/). If you would like to build from source, clone the repository:

```bash
git clone git@bitbucket.org:mbari/boxjelly
```

and then run the build command:

```bash
poetry build
```