"""
Application constants.
"""

# Application name
APP_NAME = 'BoxJelly'

# Application organization
APP_ORGANIZATION = 'MBARI'

# Network interface defaults
CONTROL_HOST = 'localhost'
CONTROL_PORT_DEFAULT = 5005
INCOMING_PORT_DEFAULT = 5562
INCOMING_TOPIC_DEFAULT = 'localization'
OUTGOING_PORT_DEFAULT = 5561
OUTGOING_TOPIC_DEFAULT = 'localization'

# Video frame rate
FRAME_RATE_DEFAULT = 29.97
