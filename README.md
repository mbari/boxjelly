![BoxJelly logo](boxjelly/assets/images/boxjelly_logo_128.png)

# BoxJelly

**BoxJelly** is a tool for viewing and editing object tracks in video.

[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Python](https://img.shields.io/badge/language-Python-blue.svg)](https://www.python.org/downloads/)

Author: Kevin Barnard, [kbarnard@mbari.org](mailto:kbarnard@mbari.org)

---

## Install

### From PyPI

BoxJelly is available on PyPI as `boxjelly`. To install, run:

```bash
pip install boxjelly
```

### From source

This project is build with Poetry. To install from source, run (in the project root):

```bash
poetry install
```

## Run

Once BoxJelly is installed, you can run it from the command line:

```bash
boxjelly
```

**You must have Cthulhu installed and running before you can use BoxJelly.**

## Documentation

Official documentation is available at [docs.mbari.org/boxjelly](https://docs.mbari.org/boxjelly/).
Alternatively, you can build the documentation from source with `mkdocs build`.

## Credits

Icons from [icons8.com](https://icons8.com/).

---

Copyright &copy; 2021&ndash;2022 [Monterey Bay Aquarium Research Institute](https://www.mbari.org)
